<?php

use App\Role;
use App\User;
use App\Category;
use Illuminate\Database\Seeder;

class BasicElements extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'role_id' => 1,
        ]);

        Role::create([
            'name' => 'SuperAdmin',
        ]);
        Role::create([
            'name' => 'Admin',
        ]);
        Role::create([
            'name' => 'Customer',
        ]);
        Category::create([
            'name' => 'uncategorized',
            'user_id' => 0,
        ]);
    }
}
