<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sc_name');
            $table->string('sc_icon')->nullable();
            $table->string('image_name')->nullable();
            $table->text('description')->nullable();
            $table->boolean('status')->default(1);
            $table->bigInteger('sct_id')->unsigned();
            $table->foreign('sct_id')->references('id')->on('subcategory_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategories');
    }
}
