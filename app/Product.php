<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Subcategory;
// use App\Models\Brand;


class Product extends Model
{
    protected $guarded = [];

    public function categories(){
        return $this->belongsToMany('App\Category');
    }

    public function brand_name(){
    	return $this->belongsTo('App\Models\Brand','brand_id')->select('name');
    }
    public function subcategory_name(){
    	return $this->belongsTo('App\Models\Subcategory','subc_id')->select('title');
    }
}
