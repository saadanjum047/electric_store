<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Category extends Model
{
    
    protected $table = 'category';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function subcategory_type(){
    	return $this->hasMany(SubcategoryType::class,'cat_id');
    }

    public function users(){
    	return $this->belongsTo(User::class,'user_id');
    }
}
