<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    
	protected $table = 'subcategories';
 	protected $primaryKey = 'id';

    protected $guarded = [];
    
    public function subcategory_type(){
    	return $this->belongsTo(SubcategoryType::class,'id');
    }

    public function subcategory_type_name(){
    	return $this->belongsTo(SubcategoryType::class,'id')->select('sct_name');
    }

    public function product(){
    	return $this->hasMany('App\Product','subc_id');
    }
}
