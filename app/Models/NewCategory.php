<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewCategory extends Model
{
    protected $table = 'new_categories';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function category(){
    	return $this->belongsTo(Category::class,'category_id');
    }
}
