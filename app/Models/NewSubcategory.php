<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewSubcategory extends Model
{
    protected $table = 'new_subcategories';
 	protected $primaryKey = 'id';

    protected $guarded = [];


    public function subcategory(){
    	return $this->belongsTo(Subcategory::class,'subcategory_id');
    }
}
