<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturedSubcategory extends Model
{
    protected $table = 'featured_subcategories';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function subcategory(){
    	return $this->belongsTo(Subcategory::class,'subcategory_id');
    }

    public function products(){
    	return $this->hasMany('App\Product','subcategory_id');
    }
}
