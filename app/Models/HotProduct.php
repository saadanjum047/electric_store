<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotProduct extends Model
{
    protected $table = 'hot_products';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function product(){
    	return $this->belongsTo(Product::class,'product_id');
    }
}
