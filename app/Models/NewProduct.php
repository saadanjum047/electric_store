<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewProduct extends Model
{
    protected $table = 'new_products';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function product(){
    	return $this->belongsTo(Product::class,'product_id');
    }
}
