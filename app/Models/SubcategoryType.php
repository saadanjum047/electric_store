<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubcategoryType extends Model
{
    protected $table = 'subcategory_types';
 	protected $primaryKey = 'id';

    protected $guarded = [];
    
    public function category(){
    	return $this->belongsTo(Category::class,'id');
    }

    public function subcategory(){
    	return $this->hasMany(Subcategory::class,'sct_id');
    }

    public function category_name(){
    	return $this->belongsTo(Category::class,'id')->select('c_name');
    }
}
