<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotSubcategory extends Model
{
    protected $table = 'hot_subcategories';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function subcategory(){
    	return $this->belongsTo(Subcategory::class,'subcategory_id');
    }
}

