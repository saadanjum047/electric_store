<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerImages extends Model
{
    protected $table = 'banner_images';
 	protected $primaryKey = 'id';

    protected $guarded = [];
}
