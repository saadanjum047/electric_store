<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Hash;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role_id' , 2)->get();
        
        return view('users.index' , compact('users') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required' ,
            'email' => 'required | unique:users' ,
            'password' => 'required' ,
            'conf_password' => 'required' ,
            'avatar' => 'sometimes'
        ]);

        // if ($request->hasFile('avatar')) {
        //     $extension = ".".$request->avatar->getClientOriginalExtension();
        //     $image = basename( $user->id , $extension);
        //     $fileName = $image.$extension;
        //     // dd($fileName);
        //     $path = public_path().'/user_avatar';
        //     $uplaod = $request->avatar->move($path , $fileName);
        //     }

        if($request->password != $request->conf_password){
            return redirect()->back()->with('error' , 'Your password does not matches');
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'avatar' => $request->avatar,
            'password' => Hash::make($request->password),
            'role_id' => 2,
        ]);

        if($user){
            return redirect('/dashboard/users')->with('success' , 'Admin created successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit' , compact('user') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=  User::findOrFail($id);

        $request->validate([
            'name' => 'required' ,
            'email' => 'required | unique:users,email,'.$user->id ,
            'password' => 'sometimes' ,
            'conf_password' => 'sometimes' ,
            'avatar' => 'sometimes',

        ]);

        if($request->password != $request->conf_password){
            return redirect()->back()->with('error' , 'Your password does not matches');
        }

        $user = User::findOrFail($id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password) ?? $user->password ,
            'avatar' => $request->avatar,
        ]);

        if($user){
            return redirect('/dashboard/users')->with('success' , 'Admin updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user->delete()){
            return redirect()->back()->with('success' , 'User deleted successfully');
        }
        // dd($user);
    }

    public function editProfile($id){
        $user = User::findOrFail($id);
        return view('profile.edit_profile' , compact('user') );
    }

    public function updateProfile($id , Request $request){
        // dd($request->all());
        $user = User::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'avatar' => 'sometimes',
            'email' => 'required | unique:users,email,'.$user->id,
        ]);
            // dd($request->all());
        if ($request->hasFile('avatar')) {
            $extension = ".".$request->avatar->getClientOriginalExtension();
            $image = basename( $user->id , $extension);
            $fileName = $image.$extension;
            // dd($fileName);
            $path = public_path().'/user_avatar';
            $uplaod = $request->avatar->move($path , $fileName);
            }
            
        $user->name = $request->name;
        $user->email = $request->email;
        $user->avatar = $fileName ?? $user->avatar;
        $user->save();

        return redirect('/dashboard')->with('success' , 'Profile Updated successfully');
        

    }
}
