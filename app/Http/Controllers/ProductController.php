<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Brand;
use App\Models\Subcategory;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index' , compact('products') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $subcategory = Subcategory::all();
        $brands = Brand::all();
        return view('products.create' , compact('categories','brands','subcategory') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'title' =>'required',
            'slug' => 'required',
            'description' => 'sometimes',
            'price' => 'required',
            'discount' => 'sometimes',
            'featured_image' => 'sometimes',
        ]);


        $product = new Product();
        $product->subc_id = $request->subc_id;
        $product->brand_id = $request->brand_id;
        $product->title = $request->title;
        $product->slug = $request->slug;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->discount = $request->discount;
        $product->product_on_sale = $request->product_on_sale;
        $product->status = $request->status;
        $product->user_id = auth()->user()->id;

        if ($request->file('featured_image')) {
            $file = $request->featured_image;
            $folder = 'products';
            $product->featured_image = saveImages($file, $folder);
        }

        $product->save();
     
        if(isset($request->categories))
        $product->categories()->attach($request->categories);
        else
        $product->categories()->attach([1]);

        if($product){
            return redirect('/admin/product')->with('success' , 'Product added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        $subcategory = Subcategory::all();
        $brands = Brand::all();
        return view('products.edit' , compact('brands','subcategory','categories', 'product') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // dd($request->all()  , $product);
        
        $request->validate([
            'title' =>'required',
            'slug' => 'required',
            'description' => 'sometimes',
            'price' => 'required',
            'discount' => 'sometimes',
            'featured_image' => 'sometimes',
        ]);
        
        $product->subc_id = $request->subc_id;
        $product->brand_id = $request->brand_id;
        $product->title = $request->title;
        $product->slug = $request->slug;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->discount = $request->discount;
        $product->product_on_sale = $request->product_on_sale;
        $product->status = $request->status;
        $product->user_id = auth()->user()->id;

        if ($request->file('featured_image')) {
            $file = $request->featured_image;
            $folder = 'products';
            $old_file_name = $product->featured_image;
            $product->featured_image = saveImages($file, $folder,$old_file_name);
        }

        $product->save();

        if(isset($request->categories))
        $product->categories()->sync($request->categories);
        else
        $product->categories()->sync([1]);

        if($product->save()){
            return redirect('/admin/product')->with('success' , 'Product updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->categories()->sync([]);
        // $product->delete();
        if($product->delete())
        return redirect()->back()->with('success' , 'Product deleted successfuly');
        else
        return redirect()->back()->with('error' , 'Some problem occoured');

    }

    public function product_on_sale($id){

        $product = Product::find($id);
        $product->product_on_sale = $product->product_on_sale == 1 ? 0 : 1;
        $product->save();
        if($product){
            return redirect()->route('product.index')->with('success' , 'Status updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }

    }   
    public function change_status($id){
    
        $product = Product::find($id);
        $product->status = $product->status == 1 ? 0 : 1;
        $product->save();
        if($product){
            return redirect()->route('products.index')->with('success' , 'Status updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }
}
