<?php

namespace App\Http\Controllers;

use App\Coupen;
use Illuminate\Http\Request;

class CoupenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupens = Coupen::all();
        return view('coupens.index' , compact('coupens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coupens.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            "name" => "required",
            "discount" => "required",
            "start" => "required",
            "end" => "required",
        ]);

        $coupen = Coupen::create([
            "name" => $request->name,
            "discount" => $request->discount,
            "start_date" => $request->start,
            "end_date" => $request->end,
            "user_id" => auth()->user()->id,
        ]);

        if($coupen){
            return redirect('/dashboard/coupens')->with('success' , 'Coupen added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupen  $coupen
     * @return \Illuminate\Http\Response
     */
    public function show(Coupen $coupen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupen  $coupen
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupen $coupen)
    {
        return view('coupens.edit' , compact('coupen'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupen  $coupen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupen $coupen)
    {
        $request->validate([
            "name" => "required",
            "discount" => "required",
            "start" => "required",
            "end" => "required",
        ]);

        $coupen->name = $request->name;
        $coupen->discount = $request->discount;
        $coupen->start_date = $request->start;
        $coupen->end_date = $request->end;
        $coupen->save();

        if($coupen->save()){
            return redirect('/dashboard/coupens')->with('success' , 'Coupen updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupen  $coupen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupen $coupen)
    {
        $coupen->delete();
        return redirect()->back()->with('success' , 'Coupen deleted successfully');
    }
}
