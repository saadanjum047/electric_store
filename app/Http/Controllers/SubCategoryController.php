<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = SubCategory::all();
        return view('sub_category.index' , compact('subcategories') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('sub_category.create' , compact('categories') );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
            'parent' => 'required',
        ]);

        $subcat = SubCategory::create([
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => $request->parent,
            'user_id' => auth()->user()->id,

        ]);

        if($subcat){
            return redirect('/dashboard/subcategory')->with('success' , 'SubCategory Added successfully');
        }else{
            return redirect()->back()->with('error' , 'some problem occoured');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $subCategory = SubCategory::findOrFail($id);
        return view('sub_category.edit' , compact('categories' ,'subCategory') );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
            'parent' => 'required',
        ]);

        $subcat = SubCategory::findOrFail($id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => $request->parent,
        ]);

        if($subcat){
            return redirect('/dashboard/subcategory')->with('success' , 'SubCategory Added successfully');
        }else{
            return redirect()->back()->with('error' , 'some problem occoured');

        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        // dd($subCategory);
        $subCategory = SubCategory::findOrFail($id);
        $subCategory->delete();
        return redirect()->back()->with('success' , 'SubCategory deleted successfully');

    }
}
