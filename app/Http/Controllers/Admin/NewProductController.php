<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubcategoryType;
use App\Models\Subcategory;
use App\Models\NewCategory;
use App\Models\NewProduct;
use App\Models\NewSubcategory;
use App\Models\NewSubcategorytype;
use App\Product;

class NewProductController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = NewCategory::with('category')->get();
        return view('admin.new-products.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        // dd($categories);
        return view('admin.new-products.form',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $categories = $request->categories;
        foreach ($categories as $key => $value) {
            array_push($data, ['category_id' => $value ]);
        }

        $category = NewCategory::insert($data);
        
        if($category){
             return redirect('/admin/new-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = NewCategory::find($id)->delete();

        if($category){
             return redirect('/admin/new-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    // subcategory type 
    
    public function features_subcatet($id){
        $subcate_type = NewSubcategorytype::with([
                'subcategory_type' => function($q) use ($id) {
                    $q->where('cat_id',$id)->get();
                }])->get();
        // dd($subcate_type);
        return view('admin.new-products.subcategory-type.index',compact('subcate_type','id'));
    }

    public function create_features_subcatet($id)
    {
        $subcategories_type = SubcategoryType::where('cat_id',$id)->get();
        // dd($subcategories_type);
        return view('admin.new-products.subcategory-type.form',compact('subcategories_type','id'));
    }

    public function store_features_subcatet(Request $request,$id)
    {
        $data = [];
        $subsubcategories_typetype = $request->subsubcategories_typetype;
        foreach ($subsubcategories_typetype as $key => $value) {
            array_push($data, ['subcategory_type_id' => $value ]);
        }

        $category = NewSubcategorytype::insert($data);
        
        if($category){
            // return back()->with('success' , 'Record store successfully');
             return redirect()->route('new.subcategory.type',$id)->with('success' , 'Record store successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    public function destroy_features_subcatet($id)
    {
        $category = NewSubcategorytype::find($id)->delete();

        if($category){
             return back()->with('success' , 'Record deleted successfully');
             return redirect('/admin/new-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    // subcategory 

    public function features_subcategory($id){
        $subcategory = NewSubcategory::with('subcategory')->get();
        return view('admin.new-products.subcategory.index',compact('subcategory','id'));
    } 

    public function create_features_subcategory($id)
    {
        $subcategories = Subcategory::where('sct_id',$id)->get();
        // dd($subcategories);
        return view('admin.new-products.subcategory.form',compact('subcategories','id'));
    }

    public function store_features_subcategory(Request $request,$id)
    {
        $data = [];
        $subcategories = $request->subcategories;
        foreach ($subcategories as $key => $value) {
            array_push($data, ['subcategory_id' => $value ]);
        }

        $category = NewSubcategory::insert($data);
        
        if($category){
            // return back()->with('success' , 'Record store successfully');
             return redirect()->route('new.subcategory',$id)->with('success' , 'Record store successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    public function destroy_features_subcategory($id)
    {
        $category = NewSubcategory::find($id)->delete();

        if($category){
             return back()->with('success' , 'Record deleted successfully');
             return redirect('/admin/new-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }


    // products
    public function features_products($id){
        $products = NewProduct::with('product')->get();
        // dd($products);
        return view('admin.new-products.products.index',compact('products','id'));
    }

    public function create_features_products($id)
    {
        $products = Product::all();
        return view('admin.new-products.products.form',compact('products','id'));
    }

    public function store_features_products(Request $request,$id)
    {
        $data = [];
        $products = $request->products;
        foreach ($products as $key => $value) {
            array_push($data, ['product_id' => $value ]);
        }

        $category = NewProduct::insert($data);
        
        if($category){
            // return back()->with('success' , 'Record store successfully');
            return redirect()->route('new.product',$id)->with('success' , 'Record store successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    public function destroy_features_products($id)
    {
     
        $category = NewProduct::find($id)->delete();

        if($category){
            return back()->with('success' , 'Record deleted successfully');
             return redirect('/admin/new-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }


}
