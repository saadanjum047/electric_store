<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HomeSlider;

class HomeSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home_slider = HomeSlider::paginate(20);
        return view('admin.home-slider.index',compact('home_slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.home-slider.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'name' => 'required',
           'image_name' => 'required',
           'status' => 'required'
        ]);

        $home_slider = new HomeSlider();
        $home_slider->name = $request->name;
        $home_slider->status = $request->status;
        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'home_slider';
            $home_slider->image_name = saveImages($file, $folder);
        }
        $home_slider->save();
        if($home_slider){
            return redirect()->route('home-slider.index')->with('success' , 'Slider Images added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $home_slider = HomeSlider::find($id);
         return view('admin.home-slider.edit',compact('home_slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
           'name' => 'required',
           'status' => 'required'
        ]);

        $home_slider = HomeSlider::find($id);
        $home_slider->name = $request->name;
        $home_slider->status = $request->status;
        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'home_slider';
            $old_file_name = $home_slider->image_name;
            $home_slider->image_name = saveImages($file, $folder,$old_file_name);
        }
        $home_slider->save();
        
        if($home_slider){
            return redirect()->route('home-slider.index')->with('success' , 'Slider Image updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $home_slider = HomeSlider::find($id);
        $home_slider->delete();
    }

    public function change_status($id){
    
        $home_slider = HomeSlider::find($id);
        $home_slider->status = $home_slider->status == 1 ? 0 : 1;
        $home_slider->save();
        if($home_slider){
            return redirect()->route('home-slider.index')->with('success' , 'Status updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }
}
