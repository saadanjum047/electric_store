<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubcategoryType;
use App\Models\Subcategory;
use App\Models\FeaturedCategory;
use App\Models\FeaturedProduct;
use App\Models\FeaturedSubcategory;
use App\Models\FeaturedSubcategorytype;
use App\Product;


class FeaturedCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = FeaturedCategory::with('category')->get();
        return view('admin.featured-products.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        // dd($categories);
        return view('admin.featured-products.form',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $categories = $request->categories;
        foreach ($categories as $key => $value) {
            array_push($data, ['category_id' => $value ]);
        }

        $category = FeaturedCategory::insert($data);
        
        if($category){
             return redirect('/admin/featured-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = FeaturedCategory::find($id)->delete();

        if($category){
             return redirect('/admin/featured-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    // subcategory type 
    
    public function features_subcatet($id){
        $subcate_type = FeaturedSubcategorytype::with([
                'subcategory_type' => function($q) use ($id) {
                    $q->where('cat_id',$id)->get();
                }])->get();
        // dd($subcate_type);
        return view('admin.featured-products.subcategory-type.index',compact('subcate_type','id'));
    }

    public function create_features_subcatet($id)
    {
        $subcategories_type = SubcategoryType::where('cat_id',$id)->get();
        return view('admin.featured-products.subcategory-type.form',compact('subcategories_type','id'));
    }

    public function store_features_subcatet(Request $request,$id)
    {
        $data = [];
        $subsubcategories_typetype = $request->subsubcategories_typetype;
        foreach ($subsubcategories_typetype as $key => $value) {
            array_push($data, ['subcategory_type_id' => $value ]);
        }

        $category = FeaturedSubcategorytype::insert($data);
        
        if($category){
            // return back()->with('success' , 'Record store successfully');
             return redirect()->route('featured.subcategory.type',$id)->with('success' , 'Record store successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    public function destroy_features_subcatet($id)
    {
        $category = FeaturedSubcategorytype::find($id)->delete();

        if($category){
             return back()->with('success' , 'Record deleted successfully');
             return redirect('/admin/featured-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    // subcategory 

    public function features_subcategory($id){
        $subcategory = FeaturedSubcategory::with('subcategory')->get();
        return view('admin.featured-products.subcategory.index',compact('subcategory','id'));
    } 

    public function create_features_subcategory($id)
    {
        $subcategories = Subcategory::where('sct_id',$id)->get();
        // dd($subcategories);
        return view('admin.featured-products.subcategory.form',compact('subcategories','id'));
    }

    public function store_features_subcategory(Request $request,$id)
    {
        $data = [];
        $subcategories = $request->subcategories;
        foreach ($subcategories as $key => $value) {
            array_push($data, ['subcategory_id' => $value ]);
        }

        $category = FeaturedSubcategory::insert($data);
        
        if($category){
            // return back()->with('success' , 'Record store successfully');
             return redirect()->route('featured.subcategory',$id)->with('success' , 'Record store successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    public function destroy_features_subcategory($id)
    {
        $category = FeaturedSubcategory::find($id)->delete();

        if($category){
             return back()->with('success' , 'Record deleted successfully');
             return redirect('/admin/featured-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }


    // products
    public function features_products($id){
        $products = FeaturedProduct::with('product')->get();
        // dd($products);
        return view('admin.featured-products.products.index',compact('products','id'));
    }

    public function create_features_products($id)
    {
        $products = Product::all();
        return view('admin.featured-products.products.form',compact('products','id'));
    }

    public function store_features_products(Request $request,$id)
    {
        $data = [];
        $products = $request->products;
        foreach ($products as $key => $value) {
            array_push($data, ['product_id' => $value ]);
        }

        $category = FeaturedProduct::insert($data);
        
        if($category){
            // return back()->with('success' , 'Record store successfully');
            return redirect()->route('featured.product',$id)->with('success' , 'Record store successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    public function destroy_features_products($id)
    {
     
        $category = FeaturedProduct::find($id)->delete();

        if($category){
            return back()->with('success' , 'Record deleted successfully');
             return redirect('/admin/featured-products')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    
      
}
