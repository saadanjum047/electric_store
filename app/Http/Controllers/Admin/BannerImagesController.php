<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BannerImages;

class BannerImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner_images = BannerImages::paginate(20);
        return view('admin.banner-images.index',compact('banner_images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner-images.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $banner_images = new BannerImages();
        $banner_images->status = $request->status;

        if ($request->file('ht_banner_image')) {
            $file = $request->ht_banner_image;
            $folder = 'banner_images';
            $banner_images->ht_banner_image = saveImages($file, $folder);
        }
        if ($request->file('hb_banner_image_1')) {
            $file = $request->hb_banner_image_1;
            $folder = 'banner_images';
            $banner_images->hb_banner_image_1 = saveImages($file, $folder);
        }
        if ($request->file('hb_banner_image_2')) {
            $file = $request->hb_banner_image_2;
            $folder = 'banner_images';
            $banner_images->hb_banner_image_2 = saveImages($file, $folder);
        }

        if ($request->file('hr_banner_image_1')) {
            $file = $request->hr_banner_image_1;
            $folder = 'banner_images';
            $banner_images->hr_banner_image_1 = saveImages($file, $folder);
        }
        if ($request->file('hr_banner_image_2')) {
            $file = $request->hr_banner_image_2;
            $folder = 'banner_images';
            $banner_images->hr_banner_image_2 = saveImages($file, $folder);
        }
        if ($request->file('hr_banner_image_3')) {
            $file = $request->hr_banner_image_3;
            $folder = 'banner_images';
            $banner_images->hr_banner_image_3 = saveImages($file, $folder);
        }
        if ($request->file('center_banner_image')) {
            $file = $request->center_banner_image;
            $folder = 'banner_images';
            $banner_images->center_banner_image = saveImages($file, $folder);
        }

        $banner_images->save();

        if($banner_images){
            return redirect()->route('banner-images.index')->with('success' , 'Banner Images added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner_images = BannerImages::find($id);
         return view('admin.banner-images.edit',compact('banner_images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
  
        $banner_images = BannerImages::find($id);
        $banner_images->status = $request->status;

        if ($request->file('ht_banner_image')) {
            $file = $request->ht_banner_image;
            $folder = 'banner_images';
            $old_file_name = $banner_images->ht_banner_image;
            $banner_images->ht_banner_image = saveImages($file, $folder,$old_file_name);
        }
        if ($request->file('hb_banner_image_1')) {
            $file = $request->hb_banner_image_1;
            $folder = 'banner_images';
            $old_file_name = $banner_images->hb_banner_image_1;
            $banner_images->hb_banner_image_1 = saveImages($file, $folder,$old_file_name);
        }
        if ($request->file('hb_banner_image_2')) {
            $file = $request->hb_banner_image_2;
            $folder = 'banner_images';
            $old_file_name = $banner_images->hb_banner_image_2;
            $banner_images->hb_banner_image_2 = saveImages($file, $folder,$old_file_name);
        }

        if ($request->file('hr_banner_image_1')) {
            $file = $request->hr_banner_image_1;
            $folder = 'banner_images';
            $old_file_name = $banner_images->hr_banner_image_1;
            $banner_images->hr_banner_image_1 = saveImages($file, $folder,$old_file_name);
        }
        if ($request->file('hr_banner_image_2')) {
            $file = $request->hr_banner_image_2;
            $folder = 'banner_images';
            $old_file_name = $banner_images->hr_banner_image_2;
            $banner_images->hr_banner_image_2 = saveImages($file, $folder,$old_file_name);
        }
        if ($request->file('hr_banner_image_3')) {
            $file = $request->hr_banner_image_3;
            $folder = 'banner_images';
            $old_file_name = $banner_images->hr_banner_image_3;
            $banner_images->hr_banner_image_3 = saveImages($file, $folder,$old_file_name);
        }

        if ($request->file('center_banner_image')) {
            $file = $request->center_banner_image;
            $folder = 'banner_images';
            $old_file_name = $banner_images->center_banner_image;
            $banner_images->center_banner_image = saveImages($file, $folder,$old_file_name);
        }
        $banner_images->save();
        
        if($banner_images){
            return redirect()->route('banner-images.index')->with('success' , 'Banner Images updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner_images = BannerImages::find($id);
        $banner_images->delete();
    }

    public function change_status($id){
    
        $banner_images = BannerImages::find($id);
        $banner_images->status = $banner_images->status == 1 ? 0 : 1;
        $banner_images->save();
        if($banner_images){
            return redirect()->route('banner-images.index')->with('success' , 'Status updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }
}
