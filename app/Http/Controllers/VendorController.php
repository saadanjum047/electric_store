<?php

namespace App\Http\Controllers;

use App\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::all();
        return view('vendors.index' , compact('vendors') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendors.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'sometimes',
            'address' => 'sometimes',
            'description' => 'sometimes',
         ]);
 
         $vendor = Vendor::create([
            'user_id' => auth()->user()->id,
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'description' => $request->description,
         ]);
 
         if($vendor){
             return redirect('/dashboard/vendors')->with('success' , 'Vendor added successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendor $vendor)
    {
        return view('vendors.edit' , compact('vendor') );
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'sometimes',
            'address' => 'sometimes',
            'description' => 'sometimes',
         ]);
 
         $Vendor = Vendor::findOrFail($id)->update([
             'name' => $request->name,
             'phone' => $request->phone,
             'name' => $request->address,
             'description' => $request->description,
         ]);
 
         if($Vendor){
             return redirect('/dashboard/vendors')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        $vendor->delete();
        return redirect()->back()->with('success' , 'Vendor deleted successfully');
    }
}
