<?php

use UniSharp\LaravelFilemanager\Lfm;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/',  function(){
//     return view('index');
// } )->name('/');

Route::get('admin/migration-command', function () {
    
    \Artisan::call('migrate');
    dd("Done");
});


Route::get('/' , 'Frontend\HomeController@index')->name('/');
// Route::get('/' , 'HomeController@index')->name('/');
Route::get('/home' , 'HomeController@showDashbaord' )->name('home');

Route::get('/admin/login' , 'HomeController@login' )->name('dashboard.login');

Route::group( ['prefix'=>'admin','as'=>'admin.' , 'middleware' => ['auth' , 'admin']  ] , function(){


    Route::get('/',  'HomeController@adminDashboard' )->name('index');
    Route::get('/user/{id}/profile' , 'UserController@editProfile')->name('user.profile');
    Route::post('/user/{id}/profile/update' , 'UserController@updateProfile')->name('user.profile.update');
    Route::resource('users', 'UserController');
    Route::resource('category', 'CategoryController');
    Route::resource('subcategory', 'SubCategoryController');
    Route::resource('product', 'ProductController');
    Route::post('product-on-sale/{id}', 'ProductController@product_on_sale')->name('product.on.sale');
    Route::post('product-status/{id}', 'ProductController@change_status')->name('product.status');
    Route::resource('vendors', 'VendorController');
    Route::resource('brands', 'BrandController');
    Route::post('brands-status/{id}', 'BrandController@change_status')->name('brands.status');
    Route::resource('delivery_boys', 'DeliveryBoyController');
    Route::resource('coupens', 'CoupenController');
    Route::resource('newsletters', 'NewsletterController');
    Route::get('media-managment' , function(){
        return view('media.media'); })->name('media-managment');
    Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['auth']], function () {
        Lfm::routes();
    });
} );


    Route::get('/', 'Frontend\HomeController@mainPage' )->name('index');
    Route::get('/about-us', 'HomeController@about_us' )->name('about-us');


    Route::group( ['prefix'=>'dashboard','as'=>'dashboard.' , 'middleware' => 'auth'  ]  , function(){
        Route::get('/' , 'HomeController@customerDashboard' )->name('index');
} );

Route::get('clear' , function(){
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    return 'clear';
} );

Route::get('test', function () {
    return view('test');
    // return view('testing');

});

Auth::routes();


// Route::get('test' , function(){
//     return view('layouts.admin');
// } );



//  admin routes 
Route::resource('admin/categories','Admin\CategoryController');
Route::post('admin/categories-status/{id}','Admin\CategoryController@change_status')->name('categories.status');
Route::resource('admin/subcategory-type','Admin\SubcategoryTypeController');
Route::post('admin/subcategory-typ-status/{id}','Admin\SubcategoryTypeController@change_status')->name('subcategory.typ.status');
Route::resource('admin/subcategories','Admin\SubcategoryController');
Route::post('admin/subcategories-status/{id}','Admin\SubcategoryController@change_status')->name('subcategories.status');

Route::resource('admin/home-slider','Admin\HomeSliderController');
Route::post('admin/home-slider-status/{id}','Admin\HomeSliderController@change_status')->name('home.slider.status');
Route::resource('admin/banner-images','Admin\BannerImagesController');
Route::post('admin/banner-images-status/{id}','Admin\BannerImagesController@change_status')->name('banner.images.status');

Route::resource('admin/products','Admin\ProdcutController');
Route::post('admin/products-status/{id}','Admin\ProdcutController@change_status')->name('products.status');

Route::resource('admin/featured-products','Admin\FeaturedCategoryController');
// featured-subcategory-type routes 
Route::get('admin/featured-subcateory-type/{id}','Admin\FeaturedCategoryController@features_subcatet')->name('featured.subcategory.type');
Route::get('admin/create-featured-subcateory-type/{id}','Admin\FeaturedCategoryController@create_features_subcatet')->name('create.featured.subcategory.type');
Route::post('admin/store-featured-subcateory-type/{id}','Admin\FeaturedCategoryController@store_features_subcatet')->name('store.featured.subcategory.type');
Route::delete('admin/destroy-featured-subcateory-type/{id}','Admin\FeaturedCategoryController@destroy_features_subcatet')->name('destroy.featured.subcategory.type');

// featured-subcategory routes 
Route::get('admin/featured-subcateory/{id}','Admin\FeaturedCategoryController@features_subcategory')->name('featured.subcategory');
Route::get('admin/create-featured-subcateory/{id}','Admin\FeaturedCategoryController@create_features_subcategory')->name('create.featured.subcategory');
Route::post('admin/store-featured-subcateory/{id}','Admin\FeaturedCategoryController@store_features_subcategory')->name('store.featured.subcategory');
Route::delete('admin/destroy-featured-subcateory/{id}','Admin\FeaturedCategoryController@destroy_features_subcategory')->name('destroy.featured.subcategory');

// featured-products routes 
Route::get('admin/featured-product/{id}','Admin\FeaturedCategoryController@features_products')->name('featured.product');
Route::get('admin/create-featured-products/{id}','Admin\FeaturedCategoryController@create_features_products')->name('create.featured.products');
Route::post('admin/store-featured-products/{id}','Admin\FeaturedCategoryController@store_features_products')->name('store.featured.products');
Route::delete('admin/destroy-featured-products/{id}','Admin\FeaturedCategoryController@destroy_features_products')->name('destroy.featured.products');



// new products routes
Route::resource('admin/new-products','Admin\NewProductController');
// featured-subcategory-type routes 
Route::get('admin/new-subcateory-type/{id}','Admin\NewProductController@features_subcatet')->name('new.subcategory.type');
Route::get('admin/create-new-subcateory-type/{id}','Admin\NewProductController@create_features_subcatet')->name('create.new.subcategory.type');
Route::post('admin/store-new-subcateory-type/{id}','Admin\NewProductController@store_features_subcatet')->name('store.new.subcategory.type');
Route::delete('admin/destroy-new-subcateory-type/{id}','Admin\NewProductController@destroy_features_subcatet')->name('destroy.new.subcategory.type');

// featured-subcategory routes 
Route::get('admin/new-subcateory/{id}','Admin\NewProductController@features_subcategory')->name('new.subcategory');
Route::get('admin/create-new-subcateory/{id}','Admin\NewProductController@create_features_subcategory')->name('create.new.subcategory');
Route::post('admin/store-new-subcateory/{id}','Admin\NewProductController@store_features_subcategory')->name('store.new.subcategory');
Route::delete('admin/destroy-new-subcateory/{id}','Admin\NewProductController@destroy_features_subcategory')->name('destroy.new.subcategory');

// featured-products routes 
Route::get('admin/new-product/{id}','Admin\NewProductController@features_products')->name('new.product');
Route::get('admin/create-new-products/{id}','Admin\NewProductController@create_features_products')->name('create.new.products');
Route::post('admin/store-new-products/{id}','Admin\NewProductController@store_features_products')->name('store.new.products');
Route::delete('admin/destroy-new-products/{id}','Admin\NewProductController@destroy_features_products')->name('destroy.new.products');


// hot products route
Route::resource('admin/hot-products','Admin\HotProductController');
// featured-subcategory-type routes 
Route::get('admin/hot-subcateory-type/{id}','Admin\HotProductController@features_subcatet')->name('hot.subcategory.type');
Route::get('admin/create-hot-subcateory-type/{id}','Admin\HotProductController@create_features_subcatet')->name('create.hot.subcategory.type');
Route::post('admin/store-hot-subcateory-type/{id}','Admin\HotProductController@store_features_subcatet')->name('store.hot.subcategory.type');
Route::delete('admin/destroy-hot-subcateory-type/{id}','Admin\HotProductController@destroy_features_subcatet')->name('destroy.hot.subcategory.type');

// featured-subcategory routes 
Route::get('admin/hot-subcateory/{id}','Admin\HotProductController@features_subcategory')->name('hot.subcategory');
Route::get('admin/create-hot-subcateory/{id}','Admin\HotProductController@create_features_subcategory')->name('create.hot.subcategory');
Route::post('admin/store-hot-subcateory/{id}','Admin\HotProductController@store_features_subcategory')->name('store.hot.subcategory');
Route::delete('admin/destroy-hot-subcateory/{id}','Admin\HotProductController@destroy_features_subcategory')->name('destroy.hot.subcategory');

// featured-products routes 
Route::get('admin/hot-product/{id}','Admin\HotProductController@features_products')->name('hot.product');
Route::get('admin/create-hot-products/{id}','Admin\HotProductController@create_features_products')->name('create.hot.products');
Route::post('admin/store-hot-products/{id}','Admin\HotProductController@store_features_products')->name('store.hot.products');
Route::delete('admin/destroy-hot-products/{id}','Admin\HotProductController@destroy_features_products')->name('destroy.hot.products');



Route::post('ajax/listingtab_/{id}','Frontend\HomeController@featured_products');



Route::get('admin/featured-subcateory/{id}','Admin\FeaturedCategoryController@features_subcategory')->name('featured.subcategory');



// Route::resource('admin/new-products','Admin\NewProductsController');
// Route::resource('admin/hot-products','Admin\HotCategoryController');
