@extends('layouts.admin')


@section('content')


<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Brands</h3>
        <a href="{{route('admin.brands.create')}}" class="btn btn-success float-right" > <i class="fas fa-plus"></i> Create Brand</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th >#</th>
              <th>Name</th>
              <th> Image </th>
              <th>Description</th>
              <th> Status </th>
              
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($brands as $k => $brand)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td>{{$brand->name}}</td>
              <td>
                <img src="{{ asset('/uploads/brands/thumbnails'.'/'.$brand->image_name ) }}" width="100px" height="100px"> 
              </td>
              <td width="50%">{{ str_limit($brand->description , 200 )}} </td>
              <td>
                <a class="status-brands btn @if($brand->status == 1) btn-success @else btn-primary @endif" href="javascript::void();" onclick="event.preventDefault();
                                     document.getElementById('status-brands-{{ $brand->id }}').submit();">
                                  {{ $brand->status == 1 ? 'Active' : 'Inactive'  }} 
                 </a>
                <form id="status-brands-{{ $brand->id }}" action="{{ route('admin.brands.status',[$brand->id]) }}" method="POST" style="display: none;">
                 @method('post')
                        @csrf
                </form> 
              </td>
              
              <td> {{$brand->created_at->toDateString() }} </td>
              <td> <a onclick="deleteCat({{$brand->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('admin.brands.edit' , $brand->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-brand-{{$brand->id}}" action="{{route('admin.brands.destroy' , $brand->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
  <script>
    function deleteCat(id){

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('delete-brand-'+id).submit();
        }
      });

    }   
  </script>
@endsection