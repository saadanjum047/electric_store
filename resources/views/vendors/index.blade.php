@extends('layouts.admin')


@section('content')


<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Vendors</h3>
        <a href="{{route('admin.vendors.create')}}" class="btn btn-success float-right" > <i class="fas fa-plus"></i> Create Vendor</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th >#</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Address</th>
              <th>Description</th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($vendors as $k => $vendor)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td>{{$vendor->name}}</td>
              <td>{{$vendor->phone}}</td>
              <td>{{$vendor->address}}</td>
              <td width="30%">{{ str_limit($vendor->description , 200 )}} </td>
              <td> {{$vendor->created_at->toDateString() }} </td>

              <td> <a onclick="deleteCat({{$vendor->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('admin.vendors.edit' , $vendor->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-vendor-{{$vendor->id}}" action="{{route('admin.vendors.destroy' , $vendor->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
  <script>
    function deleteCat(id){

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('delete-vendor-'+id).submit();
        }
      });

    }   
  </script>
@endsection