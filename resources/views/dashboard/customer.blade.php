@extends('layouts.main')


@section('styles')
    <style>
        .panel-heading{
            background-color: #5f6874 !important,
        }

    </style>

@endsection

@section('content')
    

<div class="main-container container">
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="{{route('dashboard.index')}}">Dashboard</a></li>
        
    </ul>
    
    <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <h2 class="title">Checkout</h2>
          <div class="so-onepagecheckout ">
            <div class="col-left col-sm-3">
              
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><i class="fa fa-user"></i> Your Personal Details</h4>
                </div>
                  <div class="panel-body">
                        <fieldset id="account">
                          <div class="form-group ">
                            <label for="input-payment-firstname" class="control-label">Name :</label> 
                            <label for="input-payment-firstname" class="control-label"> <strong>{{auth()->user()->name}}</strong></label> 
                          </div>
                          <div class="form-group ">
                            <label for="input-payment-firstname" class="control-label">Email :</label> 
                            <label for="input-payment-firstname" class="control-label"> <strong>{{auth()->user()->email}}</strong></label> 
                          </div>

                        </fieldset>
                      </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><i class="fa fa-book"></i> Your Address</h4>
                </div>
                  <div class="panel-body">
                        <fieldset id="address" class="required">
                          <div class="form-group">
                            <label for="input-payment-company" class="control-label">Company</label>
                            <input type="text" class="form-control" id="input-payment-company" placeholder="Company" value="" name="company">
                          </div>
                          {{--  <div class="form-group required">
                            <label for="input-payment-address-1" class="control-label">Address 1</label>
                            <input type="text" class="form-control" id="input-payment-address-1" placeholder="Address 1" value="" name="address_1">
                          </div>
                          <div class="form-group">
                            <label for="input-payment-address-2" class="control-label">Address 2</label>
                            <input type="text" class="form-control" id="input-payment-address-2" placeholder="Address 2" value="" name="address_2">
                          </div>
                          <div class="form-group required">
                            <label for="input-payment-city" class="control-label">City</label>
                            <input type="text" class="form-control" id="input-payment-city" placeholder="City" value="" name="city">
                          </div>
                          <div class="form-group required">
                            <label for="input-payment-postcode" class="control-label">Post Code</label>
                            <input type="text" class="form-control" id="input-payment-postcode" placeholder="Post Code" value="" name="postcode">
                          </div>
                          <div class="form-group required">
                            <label for="input-payment-country" class="control-label">Country</label>
                            <select class="form-control" id="input-payment-country" name="country_id">
                              <option value=""> --- Please Select --- </option>
                              <option value="244">Aaland Islands</option>
                              <option value="1">Afghanistan</option>
                              <option value="2">Albania</option>
                              <option value="3">Algeria</option>
                              <option value="4">American Samoa</option>
                              <option value="5">Andorra</option>
                              <option value="6">Angola</option>
                              <option value="7">Anguilla</option>
                              <option value="8">Antarctica</option>
                              <option value="9">Antigua and Barbuda</option>
                              <option value="10">Argentina</option>
                              <option value="11">Armenia</option>
                              <option value="12">Aruba</option>
                              <option value="252">Ascension Island (British)</option>
                              <option value="13">Australia</option>
                              <option value="14">Austria</option>
                              <option value="15">Azerbaijan</option>
                              <option value="16">Bahamas</option>
                              <option value="17">Bahrain</option>
                              
                            </select>
                          </div>
                          <div class="form-group required">
                            <label for="input-payment-zone" class="control-label">Region / State</label>
                            <select class="form-control" id="input-payment-zone" name="zone_id">
                              <option value=""> --- Please Select --- </option>
                              <option value="3513">Aberdeen</option>
                              <option value="3514">Aberdeenshire</option>
                              <option value="3515">Anglesey</option>
                              <option value="3516">Angus</option>
                              <option value="3517">Argyll and Bute</option>
                              <option value="3518">Bedfordshire</option>
                              <option value="3519">Berkshire</option>
                              <option value="3520">Blaenau Gwent</option>
                              <option value="3521">Bridgend</option>
                              <option value="3522">Bristol</option>
                              
                            </select>
                          </div>  
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" checked="checked" value="1" name="shipping_address">
                              My delivery and billing addresses are the same.</label>
                          </div>--}}
                        </fieldset>
                      </div>
              </div>
            </div>
            <div class="col-right col-sm-9">
              <div class="row">
                
                
                
                
                <div class="col-sm-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><i class="fa fa-ticket"></i> Some Other Details</h4>
                    </div>
                      <div class="panel-body row">
                        <div class="col-sm-6 ">
                        
                        </div>
                        
                        <div class="col-sm-6">
                        
                        </div>
                      </div>
                  </div>
                </div>
                
                
                
              </div>
            </div>
          </div>
        </div>
        <!--Middle Part End -->
        
    </div>
</div>

@endsection