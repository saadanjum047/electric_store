@extends('layouts.admin')


@section('content')


<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Update User</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.users.update' , $user->id)}}" method="POST">
          @method('PUT')
          @csrf
          <div class="form-group">
              <label class="col-form-label">Name</label>
              <input type="text" class="form-control" name="name" value="{{$user->name}}" required />
          </div>
          <div class="form-group">
            <label class="col-form-label">Email</label>
            <input type="email" class="form-control" name="email" value="{{$user->email}}" required/>
        </div>
        <div class="form-group">
            <label class="col-form-label">Password</label>
            <input type="password" class="form-control" name="password" value="" />
        </div>
        <div class="form-group">
            <label class="col-form-label">Confirm Password</label>
            <input type="password" class="form-control" name="conf_password" value="" />
        </div>
        <div class="form-group">
            <label class="col-form-label">Avatar</label>
            <input type="file" class="form-control" name="avatar" />
            <div> {{$user->avatar}} </div>
        </div>
        <div>
            <button class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection