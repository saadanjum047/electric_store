@extends('layouts.admin')


@section('content')


<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Newsletters</h3>
        <a href="{{route('admin.newsletters.create')}}" class="btn btn-success float-right" > <i class="fas fa-newspaper"></i> Create Newsletter</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th >#</th>
              <th>Subject</th>
              <th>Body</th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($newsletters as $k => $newsletter)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td> <a href="{{route('admin.newsletters.show' , $newsletter)}}"> {{$newsletter->subject}} </a> </td>
              <td width="50%">{{str_limit($newsletter->body , 200)}} </td>
              <td> {{$newsletter->created_at->toDateString() }} </td>

              <td> 
              {{--  <a onclick="deleteNewsletter({{$user->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('admin.newsletters.edit' , $newsletter->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>  --}}

              {{--  <form id="delete-form-{{$user->id}}" action="{{route('admin.newsletters.destroy' , $newsletter->id)}}" method="POST" style="display: none" >  --}}
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
  <script>
    function deleteNewsletter(id){

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('delete-form-'+id).submit();
        }
      })

    }   
  </script>
@endsection