@extends('layouts.admin')


@section('content')
  @include('admin.featured-products.subcategory-type.include.table')
@endsection

@section('scripts')
  <script>
    function deleteCat(id){
      
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('delete-subcategory-type-'+id).submit();
        }
      });

    }   
  </script>
@endsection