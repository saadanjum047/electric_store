<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Categories</h3>
        <a href="{{route('subcategory-type.create')}}" class="btn btn-success float-right" > <i class="fas fa-list"></i> Create Subcategory Type</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Category</th>
              <th>Icon</th>
              <th>Image</th>
              <th>Description</th>
              <th>Status</th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($subcategory_type as $k => $value)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td>{{ $value->sct_name }}</td>
        
              <td> {{ $value->category_name['c_name'] }} </td>
              <td> 
                <img src="{{ asset('/uploads/subcategory_type/icons'.'/'.$value->sct_icon ) }}">
                {{ $value->sct_icon }} 
              </td>
              <td> 
                <img src="{{ asset('/uploads/subcategory_type/thumbnails'.'/'.$value->image_name ) }}" width="100px" height="100px">
               
              </td>
              <td width="50%">{{ str_limit($value->description , 200 )}} </td>
              <td>
                <a class="status-category btn @if($value->status == 1) btn-success @else btn-primary @endif" href="javascript::void();" onclick="event.preventDefault();
                                     document.getElementById('status-category-{{ $value->id }}').submit();">
                                  {{ $value->status == 1 ? 'Active' : 'Inactive'  }} 
                 </a>
                <form id="status-category-{{ $value->id }}" action="{{ route('subcategory.typ.status',$value->id) }}" method="POST" style="display: none;">
                 @method('post')
                        @csrf
                </form> 
              </td>
              <td> {{$value->created_at->toDateString() }} </td>

              <td> <a onclick="deleteCat({{$value->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('subcategory-type.edit' , $value->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-category-{{$value->id}}" action="{{route('subcategory-type.destroy' , $value->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>