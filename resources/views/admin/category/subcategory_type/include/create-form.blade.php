

<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Create Subcategory Type</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('subcategory-type.store')}}" method="POST" enctype="multipart/form-data">
          @csrf
        
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="cat_id" class="row float-right col-form-label ">Category:</label>
        </div>
          <div class="col-sm-8">
            <select class="custom-select" name="cat_id" id="cat_id" value="{{old('cat_id')}}" required="">
              <option value="">Select Category</option>
              @if(isset($category) && !empty($category))
              @foreach($category as $key => $value)
              <option value="{{ $value->id }}"> {{ $value->c_name }} </option>
              @endforeach
              @endif
            </select>
         
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-2">
          <label for="sct_name" class="row float-right col-form-label ">Name:</label>
        </div>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="sct_name" placeholder="Name" name="sct_name" value="{{old('sct_name')}}" required>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="sct_icon" class="row float-right col-form-label "> Icon:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="sct_icon" placeholder="" name="sct_icon" value="{{old('sct_icon')}}" >
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="image_name" class="row float-right col-form-label ">Image:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="image_name " placeholder="" name="image_name" value="{{old('image_name')}}">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="status" class="row float-right col-form-label ">Status:</label>
        </div>
          <div class="col-sm-8">
            <select class="custom-select" name="status" id="status" value="{{old('status')}}" required="">
              <option value="">Select Status</option>
              <option value="1">Active</option>
              <option value="0">Inactive</option>
            </select>
         
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-2">
          <label for="inputEmail1" class="row float-right col-form-label ">Description:</label>
        </div>
          <div class="col-sm-8">
            <textarea class="form-control" rows="10" name="description" placeholder="Some description about brands(optional)">{{old('description')}}</textarea>
          </div>
        </div>
      
        <div>
            <button class="btn btn-primary float-right">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>
