<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Update Category</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('banner-images.update' , $banner_images->id )}}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="ht_banner_image" class="row float-right col-form-label ">Header Top Banner Image:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="ht_banner_image" placeholder="Name" name="ht_banner_image" value="{{old('ht_banner_image')}}" >
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="hb_banner_image_1" class="row float-right col-form-label "> Header Bottom Banner Image_1:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="hb_banner_image_1" placeholder="" name="hb_banner_image_1" value="{{old('hb_banner_image_1')}}" >
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="hb_banner_image_2" class="row float-right col-form-label ">Header Bottom Banner Image 2:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="hb_banner_image_2 " placeholder="" name="hb_banner_image_2" value="{{old('hb_banner_image_2')}}">
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-2">
          <label for="hr_banner_image_1" class="row float-right col-form-label ">Header Right Banner Image 1:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="hr_banner_image_1 " placeholder="" name="hr_banner_image_1" value="{{old('hr_banner_image_1')}}">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="hr_banner_image_2" class="row float-right col-form-label ">Header Right Banner Image 2:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="hr_banner_image_2 " placeholder="" name="hr_banner_image_2" value="{{old('hr_banner_image_2')}}">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="hr_banner_image_3" class="row float-right col-form-label ">Hader Right Banner Image 3:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="hr_banner_image_3 " placeholder="" name="hr_banner_image_3" value="{{old('hr_banner_image_3')}}">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="center_banner_image" class="row float-right col-form-label ">Center Banner Image:</label>
        </div>
          <div class="col-sm-8">
            <input type="file" class="form-control" id="center_banner_image " placeholder="" name="center_banner_image" value="{{old('center_banner_image')}}">
          </div>
        </div>

          <div class="form-group row">
            <div class="col-sm-2">
              <label for="status" class="row float-right col-form-label ">Status:</label>
            </div>
            <div class="col-sm-8">
              <select class="custom-select" name="status" id="status" value="{{old('status')}}" required="">
                <option value="">Select Status</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
              </select>
           
            </div>
          </div>

         
      
        <div>
            <button class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@section('scripts')
  <script type="text/javascript">
    $("#status").val("{{ $banner_images->status }}");
    $("#sct_id").val("{{ $banner_images->sct_id }}");
  </script>
@endsection