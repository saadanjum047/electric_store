<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Products</h3>
        <a href="{{route('create.new.products',$id)}}" class="btn btn-success float-right" > <i class="fas fa-list"></i> Create Product</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Brand</th>
              <th>Subcategory</th>
              <th>Image</th>
              <th>Description</th>
              <th>Status</th>
              <th>Action</th>
            </tr> 
          </thead>
          <tbody>
              @foreach ($products as $k => $value)
              {{-- @php
                dd($value->product->brand_name);
              @endphp --}}
              @if(!empty($value->product))
            <tr>
              <td>{{ $k+1 }}.</td>
              <td>{{ $value->product->title }}</td>
              <td> {{ $value->product->brand_name }} </td>
              <td> {{ $value->product->subcategory_name }} </td>
              <td> 
                <img src="{{ asset('/uploads/products/thumbnails'.'/'.$value->product->new_image ) }}" width="100px" height="100px">
               
              </td>
              <td>{{ str_limit($value->product->description , 200 )}} </td>
              <td>
                <a class="status-product btn @if($value->status == 1) btn-success @else btn-primary @endif" href="javascript::void();" onclick="event.preventDefault();
                                     document.getElementById('status-product-{{ $value->id }}').submit();">
                                  {{ $value->status == 1 ? 'Active' : 'Inactive'  }} 
                 </a>
                <form id="status-product-{{ $value->id }}" action="{{ route('subcategories.status',$value->product->id) }}" method="POST" style="display: none;">
                 @method('post')
                        @csrf
                </form> 
              </td>
    

              <td>
            
                <a onclick="deleteCat({{ $value->id }})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> 
                </a> 
               
              <a href="{{route('products.edit' , $value->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-products-{{ $value->id }}" action="{{route('destroy.new.products' , $value->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endif
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>