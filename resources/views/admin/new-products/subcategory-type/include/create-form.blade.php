

<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
@section('styles')
<link rel="stylesheet" href="{{asset('/admin-plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('/admin-plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

@endsection

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">New SubCategory Type</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('store.new.subcategory.type',$id)}}" method="POST" enctype="multipart/form-data">
          @csrf
       <div class="form-group row">
          <div class="col-sm-2">
          <label for="sc_name" class="row float-right col-form-label ">SubCategory Type:</label>
        </div>
          <div class="col-sm-8">
            <select class="select2bs4" multiple id="e1" name="subsubcategories_typetype[]" data-placeholder="Select Categories" style="width: 100%;">
          @foreach($subcategories_type as $category)
            <option value="{{$category->id}}"> {{$category->sct_name}} </option>
          @endforeach
        </select> 
          </div>
        </div>

      <hr>

    
        <div>
            <button class="btn btn-primary float-right">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>


@section('scripts')

<script src=" {{asset('/admin-plugins/select2/js/select2.full.min.js')}}"></script>
<script>

  $('.select2bs4').select2({
    theme: 'bootstrap4',
  });

  $("#e1").select2();

</script>

@endsection