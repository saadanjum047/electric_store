@extends('layouts.admin')


@section('content')


<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Create Category</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.subcategory.store')}}" method="POST">
          @csrf
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name')}}" required>
            </div>
          </div>
        
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Description:</label>
          </div>
            <div class="col-sm-8">
              <textarea class="form-control" rows="10" name="description" placeholder="Some description about brands(optional)">{{old('description')}}</textarea>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Parent Category:</label>
          </div>
            <div class="col-sm-8">
              <select class="form-control" name="parent" required>
                <option value="" selected> Select Parent Category </option>
                @foreach ($categories as $category)
                <option value="{{$category->id}} "> {{$category->name}} </option>
                @endforeach
              </select>
            </div>
          </div>

        <div>
            <button class="btn btn-primary float-right">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection