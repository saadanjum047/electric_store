<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Basic page needs
============================================ -->
    <title>Market - Premium Multipurpose HTML5/CSS3 Theme</title>
    <meta charset="utf-8">
    <meta name="keywords" content="boostrap, responsive, html5, css3, jquery, theme, multicolor, parallax, retina, business" />
    <meta name="author" content="Magentech">
    <meta name="robots" content="index, follow" />
    <!-- Mobile specific metas
============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon
============================================ -->
    <link rel="shortcut icon" href="ico/favicon.png">
    <!-- Google web fonts
============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
    <!-- Libs CSS
============================================ -->
    <link rel="stylesheet" href="{{('main-css/bootstrap/css/bootstrap.min.css')}}">
    <link href="{{('main-css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{('main-js/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{('main-js/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{('main-css/themecss/lib.css')}}" rel="stylesheet">
    <link href="{{('main-js/jquery-ui/jquery-ui.min.css')}}" rel="stylesheet">
    <!-- Theme CSS
============================================ -->
    <link href="{{('main-css/themecss/so_megamenu.css')}}" rel="stylesheet">
    <link href="{{('main-css/themecss/so-categories.css')}}" rel="stylesheet">
    <link href="{{('main-css/themecss/so-listing-tabs.css')}}" rel="stylesheet">
    <link href="{{('main-css/footer1.css')}}" rel="stylesheet">
    <link href="{{('main-css/header2.css')}}" rel="stylesheet">
    <link id="color_scheme" href="{{('main-css/home2.css')}}" rel="stylesheet">
    <link href="{{('main-css/responsive.css')}}" rel="stylesheet"> </head>