<!-- Block Spotlight2  -->
	<section class="so-spotlight2">
		<div class="container">
			<div class="row">
				<div class="news-letter col-md-3 col-sm-12 col-xs-12">
					<div class="newsletter">
						<h2>NewsLetter</h2>
						<p class="page-heading-sub hidden-md hidden-sm"> Please sign up to the Market mailing list to receive updates on new arrivals, special offers and other discount information </p>
						<form action="#" method="post">
							<div class="form-group required">
								<div class="input-box">
									<input type="email" name="txtemail" id="txtemail" value="" placeholder="" class="form-control input-lg"> </div>
								<div class="subcribe">
									<button type="submit" class="btn btn-default btn-lg" onclick="return subscribe();">Subscribe</button> <span>Subscribe</span> </div>
							</div>
						</form>
					</div>
				</div>
				<div class="banner-html  hidden-xs col-md-9 col-sm-12 col-xs-12">
					<div class="module">
						<div class="modcontent clearfix">
							<div class="m-banner">
								<div class="m-banner-right">
									<div class="m-banner2 banners">
										<div>
											<a href="#"><img src="{{ asset($data['ht_banner_image']) }}" alt="banner1"></a>
										</div>
									</div>
									
									<div class="m-banner34">
										<div class="m-banner3 banners">
											<div>
												<a href="#"><img src="{{ asset($data['hb_banner_image_1']) }}" alt="banner1"></a>
											</div>
										</div>
										<div class="m-banner4 banners ">
											<div>
												<a href="#"><img src="{{ asset($data['hb_banner_image_2']) }}" alt="banner1"></a>
											</div>
										</div>
										
									</div>
								</div>
								<div class="m-banner1 banners hidden-xs">
									<div>
										<a href="#"><img src="{{ asset($data['hr_banner_image_1']) }}" alt="banner1"></a>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //Block Spotlight2  -->