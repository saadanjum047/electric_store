<!-- Block Spotlight3  -->
	<section class="so-spotlight3">
		<div class="container">
			<div class="row">
				
				<div id="so_categories_173761471880018" class="so-categories module titleLine preset01-4 preset02-3 preset03-3 preset04-1 preset05-1">
					<h3 class="modtitle">Hot Categories</h3>
					<div class="col-md-12">
										<div class="carousel slide" id="myCarousel_1">
										  <div class="carousel-inner">
										  	@if(isset($hot_categories) && !empty($hot_categories))
							                    	@foreach($hot_categories as $key => $value)
										    <div class="item @if($key == 0) active @endif">
										      <div class="col-sm-3">
						                            <div class="col-item">
						                            	<div class="image-cat">
															<a href="#" title="Automotive" >
																<img src="image/demo/shop/category/automotive-motocrycle.jpg" title="Automotive" alt="Automotive"> 
															</a> 
															<a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 								
														</div>
						                            </div>
						                        </div>
										    </div>
										    @endforeach
							              @endif
										    <div class="item">
										      <div class="col-sm-3">
						                            <div class="col-item">
						                            	<div class="image-cat">
															<a href="#" title="Automotive" >
																<img src="image/demo/shop/category/automotive-motocrycle.jpg" title="Automotive" alt="Automotive"> 
															</a> 
															<a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 								
														</div>
						                            </div>
						                        </div>
										    </div>
										    <div class="item">
										      <div class="col-sm-3">
						                            <div class="col-item">
						                            	<div class="image-cat">
															<a href="#" title="Automotive" >
																<img src="image/demo/shop/category/automotive-motocrycle.jpg" title="Automotive" alt="Automotive"> 
															</a> 
															<a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 								
														</div>
						                            </div>
						                        </div>
										    </div>
										    <div class="item">
										      <div class="col-sm-3">
						                            <div class="col-item">
						                            	<div class="image-cat">
															<a href="#" title="Automotive" >
																<img src="image/demo/shop/category/automotive-motocrycle.jpg" title="Automotive" alt="Automotive"> 
															</a> 
															<a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 								
														</div>
						                            </div>
						                        </div>
										    </div>
										    <div class="item">
										      <div class="col-sm-3">
						                            <div class="col-item">
						                            	<div class="image-cat">
															<a href="#" title="Automotive" >
																<img src="image/demo/shop/category/automotive-motocrycle.jpg" title="Automotive" alt="Automotive"> 
															</a> 
															<a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 								
														</div>
						                            </div>
						                        </div>
										    </div>
										    <div class="item">
										      <div class="col-sm-3">
						                            <div class="col-item">
						                            	<div class="image-cat">
															<a href="#" title="Automotive" >
																<img src="image/demo/shop/category/automotive-motocrycle.jpg" title="Automotive" alt="Automotive"> 
															</a> 
															<a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 								
														</div>
						                            </div>
						                        </div>
										    </div>
										  </div>
										  <a class="left carousel-control" href="#myCarousel_1" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
										  <a class="right carousel-control" href="#myCarousel_1" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
										</div>
									</div>

				</div>
			</div>
		</div>
	</section>
	<!-- //Block Spotlight3  -->