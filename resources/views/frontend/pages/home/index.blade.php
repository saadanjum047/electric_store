@extends('frontend.master-layout')
@push('css')

@endpush

@section('content')
	@include('frontend.pages.home.includes.block-spotlight-01')
	@include('frontend.pages.home.includes.block-spotlight-02')
	@include('frontend.pages.home.includes.main-container')
	@include('frontend.pages.home.includes.block-spotlight-03')
@endsection

@push('js')

@endpush