<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Basic page needs
	============================================ -->
	<title>Market - Premium Multipurpose HTML5/CSS3 Theme</title>
	<meta charset="utf-8">
    <meta name="keywords" content="boostrap, responsive, html5, css3, jquery, theme, multicolor, parallax, retina, business" />
    <meta name="author" content="Magentech">
    <meta name="robots" content="index, follow" />
   
	<!-- Mobile specific metas
	============================================ -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	<!-- Favicon
	============================================ -->
    <link rel="shortcut icon" href="{{ asset('frontend-assets') }}/ico/favicon.png">
	
	<!-- Google web fonts
	============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
	
    <!-- Libs CSS
	============================================ -->
    <link rel="stylesheet" href="{{ asset('frontend-assets') }}/css/bootstrap/css/bootstrap.min.css">
	<link href="{{ asset('frontend-assets') }}/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="{{ asset('frontend-assets') }}/js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="{{ asset('frontend-assets') }}/js/owl-carousel/owl.carousel.css" rel="stylesheet">
	<link href="{{ asset('frontend-assets') }}/css/themecss/lib.css" rel="stylesheet">
	<link href="{{ asset('frontend-assets') }}/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
	
	<!-- Theme CSS
	============================================ -->
   	<link href="{{ asset('frontend-assets') }}/css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="{{ asset('frontend-assets') }}/css/themecss/so-categories.css" rel="stylesheet">
	<link href="{{ asset('frontend-assets') }}/css/themecss/so-listing-tabs.css" rel="stylesheet">
	<link href="{{ asset('frontend-assets') }}/css/footer1.css" rel="stylesheet">
	<link href="{{ asset('frontend-assets') }}/css/header2.css" rel="stylesheet">
	<link id="color_scheme" href="{{ asset('frontend-assets') }}/css/home2.css" rel="stylesheet">
	<link href="{{ asset('frontend-assets') }}/css/responsive.css" rel="stylesheet">
	

	@stack('css')
</head>















			
