<!-- Header Top -->
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="header-top-left form-inline col-lg-6 col-md-5 col-sm-6 compact-hidden hidden-sm hidden-xs">
					<div class="form-group navbar-welcome " >
											
					</div>
				</div>
				<div class="header-top-right collapsed-block text-right col-lg-6 col-md-7 col-sm-12 col-xs-12 compact-hidden">
					<h5 class="tabBlockTitle visible-xs">More<a class="expander " href="#TabBlock-1"><i class="fa fa-angle-down"></i></a></h5>
					<div class="tabBlock" id="TabBlock-1">
						<ul class="top-link list-inline">
							<li class="account btn-group" id="my_account">
								<a href="#" title="My Account" class="btn btn-xs dropdown-toggle" data-toggle="dropdown"> <span class="hidden-xs">My Account</span> <span class="fa fa-angle-down "></span></a>
								<ul class="dropdown-menu ">
									<li><a href="register.html"><i class="fa fa-user"></i> Register</a></li>
									<li><a href="login.html"><i class="fa fa-pencil-square-o"></i> Login</a></li>
								</ul>
							</li>
							<li class="wishlist"><a href="wishlist.html" id="wishlist-total" class="top-link-wishlist" title="Wish List (2)"><span class="hidden-xs">Wish List (2)</span></a></li>
							<li class="checkout"><a href="checkout.html" class="top-link-checkout" title="Checkout"><span class="hidden-xs">Checkout</span></a></li>
							
						</ul>
						<div class="form-group languages-block ">
							<form action="https://demo.smartaddons.com/templates/html/market/index.html" method="post" enctype="multipart/form-data" id="bt-language">
								
							</form>
						</div>

						
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //Header Top -->