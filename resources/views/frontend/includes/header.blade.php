<body class="common-home res layout-home1">
	
    <div id="wrapper" class="wrapper-full banners-effect-7">

	<!-- Header Container  -->
	<header id="header" class=" variantleft type_2">

	@include('frontend.includes.header-top')
	@include('frontend.includes.header-center')
	@include('frontend.includes.header-bottom')